#!/usr/local/bin/perl
#
######################################################################
###
###
###  CGIフォームメール送信 T-FormMail Ver.1.03
###     [2/3] 管理用 (tfmadmin.cgi)
###                                 (c) 1996-2002 Takahiro Nishida
###                                 http://www.mytools.net/
###
###
######################################################################

### 変数設定部 （詳細は上記ページをご覧下さい） ######################

$password = "TFMail103";

### 変数設定部 （ここまで）###########################################

require './tfmlib.pl';

print "Content-type: text/html\n\n";

&main;

sub main{
	&tfmlib'lock;
	&init_variables;
	&open_datafile;
	&check_input;
	&exec_action;
	&update_files;
	&show_html;
	&tfmlib'unlock;
}



########## 変数の初期化
sub init_variables{
	$basedir = $tfmlib'basedir;
	$datafile = "$basedir/tfmdata.txt";
	$logfile = "$basedir/tfmlog.txt";
	$backurl = $tfmlib'backurl;
	
	$noact = "何も変更されませんでした。";
	$actmsg = "パスワードが違います。$noact";
}



########## データファイルのオープン
sub open_datafile{
	&tfmlib'openfile($datafile, *datas);
}



########## 入力内容のチェック
sub check_input{
	&tfmlib'parseform;
	
	$pwd = $tfmlib'F{'pwd'};
	
	$mode = $tfmlib'F{'mode'};
	$act = $tfmlib'F{'act'};
	$fcode = $tfmlib'F{'fcode'};
	(!$fcode) || ($fcode =~ /^\w+$/) || &tfmlib'error(8, "コード");
}



########## 作業の実行
sub exec_action{
	### パスワードチェック
	if ($pwd ne $password){
		$actmsg = ($pwd) ? "パスワードが間違っています。$noact" : "作業を選択し、パスワードを入力して「作業実行」を押してください。";
		$mode = "top";
		return;
	}
	
	### modeごとの処理へ
	if($mode eq 'data'){
		$actmsg = "登録済の情報を表\示しています。";
		if($act eq 'new' || $act eq 'fix'){
			&fix_data;
		}
	}
	elsif($mode eq 'log'){
		$actmsg = "最新 $tfmlib'log_max 件の送信ログを表\示しています。";
	}
}



########## データの修正
sub fix_data{
	### 入力の受け取り
	$fsendto = &tfmlib'decode($tfmlib'F{'fsendto'});
	$ffsend = $tfmlib'F{'ffsend'};
	$ffemail = $tfmlib'F{'ffemail'};
	$ffshow = $tfmlib'F{'ffshow'};
	$ffhost = $tfmlib'F{'ffhost'};
	$fmname = &tfmlib'decode($tfmlib'F{'fmname'});
	$fmname = &tfmlib'TagToValue($fmname);
	$fbackurl = &tfmlib'decode($tfmlib'F{'fbackurl'});
	$fsubject = &tfmlib'decode($tfmlib'F{'fsubject'});
	$ffinmsg = &tfmlib'decode($tfmlib'F{'ffinmsg'});
	$fcolor = &tfmlib'decode($tfmlib'F{'fcolor'});
	$fdel = $tfmlib'F{'fdel'};
	
	### 項目詳細の改行をスペースに変換
	$fmname =~ s/\n+/\ /g;

	### 入力のチェック
	($fcode) || &tfmlib'error(8, "コード");
	($fsendto =~ /^[\w\-\+\.]+\@[\w\-\+\.]+$/) || &tfmlib'error(8, "送り先");
	($ffsend =~ /^[10]$/) || &tfmlib'error(8, "E-Mail必須");
	($ffemail =~ /^[10]$/) || &tfmlib'error(8, "送り先表示");
	($ffshow =~ /^[10]$/) || &tfmlib'error(8, "メール表示");
	($ffhost =~ /^[10]$/) || &tfmlib'error(8, "ホスト記録");
	($fmname =~ /^(\w+###.+###[01]###.*\s*)+$/) || &tfmlib'error(8, "項目詳細 = $fmname");
	($fbackurl =~ /^http:\/\/.+$/) || &tfmlib'error(8, "戻り先");
	
	### 色が未入力の場合デフォルト
	($fcolor) || ($fcolor = "#FFFFFF,#000000,#FF0000,#000099,");
	
	### 書換対象の行番号
	$target = -1; 
	for(0..$#datas){
		($datas[$_] =~ /^$fcode\t/) && ($target = $_);
	}
	if($act eq 'new'){
		($target == -1) || &local_error("指定されたコード($fcode)は既に使用されています");
		$target = $#datas + 1;
		$actmsg = "$fcode を新規作成しました。";
	}
	else{
		($target != -1) || &local_error("指定されたコード($fcode)の項目が見つかりません");
		$actmsg = "$fcode を修正しました。";
	}
	
	### 削除
	if($fdel){
		splice(@datas, $target, 1);
		$actmsg = "$fcode を削除しました。";
	}
	### 修正、新規
	else{
		$newline = "$fcode\t$fsendto\t$ffsend$ffemail$ffshow$ffhost\t$fmname\t$fbackurl\t$fsubject\t$ffinmsg\t$fcolor\t\n";
		$datas[$target] = $newline;
	}
	
	$flag_data = 1;
}



########## ファイルの更新
sub update_files{
	($flag_data) && &tfmlib'updatefile($datafile, *datas);
}




########## 表示
sub show_html{
	if($mode eq 'data'){
		$tablebuf = &html_table;
		$formbuf = &html_form;
	}
	elsif($mode eq 'log'){
		$tablebuf = &html_log;
	}
	else{
		$formbuf = &html_top;
	}
	
	$hcopy = &tfmlib'copyright;
	
	print "
	<HTML>
	<HEAD><TITLE>T-FormMail - 管理用ページ</TITLE></HEAD>
	<BODY BGCOLOR=\"#CCFFCC\">
	<FONT SIZE=\"2\">
	[<A HREF=\"$backurl\">戻る</A>]
	[<A HREF=\"tfmadmin.cgi\">管理トップ</A>]
	
	<H3>T-FormMail 管理用ページ</H3>
	<HR SIZE=\"1\" NOSHADE>
	<B>作業状況：</B> $actmsg
	<HR SIZE=\"1\" NOSHADE>
	$formbuf
	$tablebuf
	<HR SIZE=\"1\" NOSHADE>
	<DIV ALIGN=\"right\">$hcopy</DIV>
	</BODY>
	</HTML>
	";
}



########## トップ画面
sub html_top{
	"
	<FORM METHOD=\"post\" ACTION=\"tfmadmin.cgi\">
	パスワード<INPUT TYPE=\"password\" NAME=\"pwd\" SIZE=\"15\">
	<INPUT TYPE=\"submit\" VALUE=\"作業実行\"><BR>
	<INPUT TYPE=\"radio\" NAME=\"mode\" VALUE=\"data\" CHECKED>メールフォーム新規作成、修正、削除<BR>
	<INPUT TYPE=\"radio\" NAME=\"mode\" VALUE=\"log\">送信ログ閲覧
	</FORM>
	";
}



########## 修正用フォーム
sub html_form{
	local($htmlbuf, $dact, $dtitle, $dcode);
	local($code, $sendto, $frags, $mname, $backurl, $subject, $finmsg, $scode, $schk);
	local($fsend, $femail, $fshow, $fhost);
	local($csend1, $csend0, $cemail1, $cemail0, $cshow1, $cshow0, $chost1, $chost0);
	
	foreach (@datas){
		$scode = (split("\t"))[0];
		($schk) = ($scode eq $fcode) ? "SELECTED" : "";
		$options .= "<OPTION " . $schk . ">" . $scode . "\n";
	}
	
	$htmlbuf .= "
	<FORM METHOD=\"post\" ACTION=\"tfmadmin.cgi\">
	<INPUT TYPE=\"hidden\" NAME=\"pwd\" VALUE=\"$pwd\">
	<INPUT TYPE=\"hidden\" NAME=\"mode\" VALUE=\"data\">
	<DIV ALIGN=\"center\">
	<TABLE WIDTH=\"60%\" BORDER=\"1\" BGCOLOR=\"#005500\">
	<TR><TH><FONT COLOR=\"#FFFFFF\">
	コード
	<SELECT NAME=\"fcode\" SIZE=\"1\">
	$options
	</SELECT>
	を
	<INPUT TYPE=\"submit\" NAME=\"dfix\" VALUE=\"読み込み\">
	<INPUT TYPE=\"submit\" NAME=\"dnew\" VALUE=\"新規登録\">
	</FONT></TD></TR></TABLE>
	</DIV>
	</FORM>
	<P>
	";
	
	### 読み込みの場合
	if($tfmlib'F{'dfix'} || $newline){
		($fcode) || &tfmlib'error(8, "コード");
		$dact = "fix";
		$dtitle = "$fcode 修正";
		$dcode = "$fcode <INPUT TYPE=\"hidden\" NAME=\"fcode\" VALUE=\"$fcode\">";
		foreach (@datas){
			(/^$fcode\t/) && (($code, $sendto, $frags, $mname, $backurl, $subject, $finmsg, $color) = split("\t"));
		}
		($fsend, $femail, $fshow, $fhost) = split("", $frags);
		($sendto) || &tfmlib'error(8, "コード");
	}
	### 新規作成の場合
	else{
		$dact = "new";
		$dtitle = "新規登録";
		$dcode = "<INPUT TYPE=\"text\" NAME=\"fcode\" SIZE=\"15\">";
	}
	
	### mnameをデコード
	$mname = &tfmlib'decode($mname);
	$mname =~ s/\s+/\n/g;
	
	### ラジオボタンのチェック位置
	($fsend)  ? ($csend1  = "CHECKED") : ($csend0  = "CHECKED");
	($femail) ? ($cemail1 = "CHECKED") : ($cemail0 = "CHECKED");
	($fshow)  ? ($cshow1  = "CHECKED") : ($cshow0  = "CHECKED");
	($fhost)  ? ($chost1  = "CHECKED") : ($chost0  = "CHECKED");
	
	$htmlbuf .= "
	<FORM METHOD=\"post\" ACTION=\"tfmadmin.cgi\">
	<INPUT TYPE=\"hidden\" NAME=\"pwd\" VALUE=\"$pwd\">
	<INPUT TYPE=\"hidden\" NAME=\"mode\" VALUE=\"data\">
	<INPUT TYPE=\"hidden\" NAME=\"act\" VALUE=\"$dact\">
	<DIV ALIGN=\"center\">
	<TABLE WIDTH=\"90%\" BORDER=\"1\" CELLPADDING=\"3\" CELLSPACING=\"0\" BGCOLOR=\"#FFFFFF\">
	<TR BGCOLOR=\"#005500\"><TH COLSPAN=\"2\"><FONT COLOR=\"#FFFFFF\">■ $dtitle ■</FONT></TH></TR>
	<TR><TH BGCOLOR=\"#FFFFCC\" NOWRAP>コード</TH>
	<TD>$dcode</TD></TR>
	<TR><TH BGCOLOR=\"#FFFFCC\" NOWRAP>送り先</TH>
	<TD><INPUT TYPE=\"text\" NAME=\"fsendto\" SIZE=\"50\" VALUE=\"$sendto\"></TD></TR>
	<TR><TH BGCOLOR=\"#FFFFCC\" NOWRAP>スイッチ</TH>
	<TD><FONT SIZE=\"2\">
	・確認時に送信先メールアドレスを… [
	<INPUT TYPE=\"radio\" NAME=\"ffsend\" VALUE=\"0\" $csend0>表\示しない
	<INPUT TYPE=\"radio\" NAME=\"ffsend\" VALUE=\"1\" $csend1>表\示する ]<BR>
	・メールアドレスの入力は… [
	<INPUT TYPE=\"radio\" NAME=\"ffemail\" VALUE=\"0\" $cemail0>なくてもよい
	<INPUT TYPE=\"radio\" NAME=\"ffemail\" VALUE=\"1\" $cemail1>必須 ]<BR>
	・送信後にメール全文を… [
	<INPUT TYPE=\"radio\" NAME=\"ffshow\" VALUE=\"0\" $cshow0>見せない
	<INPUT TYPE=\"radio\" NAME=\"ffshow\" VALUE=\"1\" $cshow1>見せる ]<BR>
	・アクセス元ホストを… [
	<INPUT TYPE=\"radio\" NAME=\"ffhost\" VALUE=\"0\" $chost0>取得しない
	<INPUT TYPE=\"radio\" NAME=\"ffhost\" VALUE=\"1\" $chost1>取得する ]<BR>
	</TD>
	</TR>
	<TR><TH BGCOLOR=\"#FFFFCC\" NOWRAP>項目詳細</TH>
	<TD>
	<TEXTAREA NAME=\"fmname\" SIZE=\"50\" COLS=\"50\" ROWS=\"5\">$mname</TEXTAREA><BR>
	<FONT SIZE=\"2\">※ 「NAME###項目名###必須###正規表\現」を<B>改行区切り</B>で繰り返す。</FONT>
	</TD></TR>
	<TR><TH BGCOLOR=\"#FFFFCC\" NOWRAP>戻り先</TH>
	<TD><INPUT TYPE=\"text\" NAME=\"fbackurl\" SIZE=\"50\" VALUE=\"$backurl\"></TD></TR>
	<TR><TH BGCOLOR=\"#FFFFCC\" NOWRAP>題名</TH>
	<TD><INPUT TYPE=\"text\" NAME=\"fsubject\" SIZE=\"50\" VALUE=\"$subject\"><BR>
	<FONT SIZE=\"2\">※ これがメールの「題名(Subject)」になります。</FONT>
	</TD></TR>
	<TR><TH BGCOLOR=\"#FFFFCC\" NOWRAP>文章</TH>
	<TD><INPUT TYPE=\"text\" NAME=\"ffinmsg\" SIZE=\"60\" VALUE=\"$finmsg\"><BR>
	<FONT SIZE=\"2\">※ 送信終了時に出るメッセージ。</FONT>
	</TD></TR>
	<TR><TH BGCOLOR=\"#FFFFCC\" NOWRAP>配色</TH>
	<TD><INPUT TYPE=\"text\" NAME=\"fcolor\" SIZE=\"60\" VALUE=\"$color\"><BR>
	<FONT SIZE=\"2\">※ 「背景色、文字色、強調文字色、リンク色、背景画像」の順に<B>半角カンマ「,」区切り</B>で入力。<BR>※ 空欄にすると標準の配色に戻る。</FONT>
	</TD></TR>
	
	<TR BGCOLOR=\"#005500\"><TH COLSPAN=\"2\"><FONT COLOR=\"#FFFFFF\">
	<INPUT TYPE=\"checkbox\" NAME=\"fdel\" VALUE=\"1\">この項目を削除　　　
	<INPUT TYPE=\"submit\" VALUE=\"作業実行\">
	</FONT></TH></TR></TABLE>
	</DIV>
	</FORM>
	<BR>
	<BR>
	<HR WIDTH=\"50%\">
	<BR>
	<BR>
	";
	
}



########## 一覧表
sub html_table{
	local($htmlbuf, $code, $sendto, $frags, $mname, $backurl, $subject, $finmsg, $i, $bgs);
	
	$htmlbuf = "
	<DIV ALIGN=\"center\">
	<TABLE WIDTH=\"80%\" BORDER=\"1\" CELLPADDING=\"1\" CELLSPACING=\"0\">
	<CAPTION><B>登録情報一覧</B></CAPTION>
	<TR BGCOLOR=\"005500\">
	<TH><FONT COLOR=\"#FFFFFF\">code</FONT></TH>
	<TH><FONT COLOR=\"#FFFFFF\">送信先</FONT></TH>
	<TH><FONT COLOR=\"#FFFFFF\">スイッチ</FONT></TH>
	</TR>
	";
	
	$i = 0;
	$bgs[0] = "#FFFFCC";
	$bgs[1] = "#FFFFFF";
	
	foreach (@datas){
		($code, $sendto, $frags, $mname, $backurl, $subject, $finmsg, $color) = split("\t");
		$mname =~ s/\+/ /g;
		$mname = &tfmlib'decode($mname);
		
		$htmlbuf .= "
		<TR BGCOLOR=\"$bgs[$i%2]\">
		<TH><FONT COLOR=\"#FF0000\">$code</FONT></TH><TD>$sendto</TD><TD>$frags</TD></TR>
		<TR BGCOLOR=\"$bgs[$i%2]\">
		<TD COLSPAN=\"3\"><FONT SIZE=\"2\">
		・<B>[必須]</B> $mname<BR>
		・<B>[戻り先]</B> $backurl<BR>
		・<B>[題名]</B> $subject<BR>
		・<B>[文章]</B> $finmsg<BR>
		・<B>[配色]</B> $color<BR>
		</FONT></TD></TR>
		";
		
		$i++;
	}
	
	$htmlbuf .= "</TABLE></DIV>";
}



########## 送信ログ
sub html_log{
	local($htmlbuf, @logs, @bgs, $i);
	local($code, $id, $time, $email, $host);
	&tfmlib'openfile($logfile, *logs);
	
	$htmlbuf .="
	<DIV ALIGN=\"center\">
	<TABLE WIDTH=\"80%\" BORDER=\"0\" CELLPADDING=\"5\" CELLSPACING=\"0\">
	<CAPTION><DIV ALIGN=\"left\">
	<H3>送信ログ</H3>
	<FONT SIZE=\"2\">
	<UL STYLE=\"list-style-type:none\">
	<LI>※ 上にいくほど最新です
	<LI>※ 「送信確認」は、入力内容確認画面（実際に送信する前）を表\示したものです。
	</UL></FONT>
	</DIV></CAPTION>
	<TR BGCOLOR=\"005500\">
	<TH><FONT COLOR=\"#FFFFFF\">code</FONT></TH>
	<TH><FONT COLOR=\"#FFFFFF\">送信ID</FONT></TH>
	<TH><FONT COLOR=\"#FFFFFF\">時刻</FONT></TH>
	<TH><FONT COLOR=\"#FFFFFF\">ホスト</FONT></TH>
	<TH><FONT COLOR=\"#FFFFFF\">E-Mail</FONT></TH>
	</TR>
	";
	
	$i = 0;
	$bgs[0] = "#FFFFCC";
	$bgs[1] = "#FFFFFF";
	
	foreach(@logs){
		($code, $id, $time, $host, $email) = split("\t");
		if($email eq "prev"){
			$email = "（送信確認）";
		}
		
		$htmlbuf .= "
		<TR BGCOLOR=\"$bgs[$i%2]\">
		<TH><FONT COLOR=\"#FF0000\">$code</FONT></TH>
		<TD>$id</TD>
		<TD>$time</TD>
		<TD>$host</TD>
		<TD>$email</TD>
		</TR>
		";
		
		$i++;
	}
	
	$htmlbuf .= "</TABLE></DIV>";
}



########## 特有のエラー
sub local_error{
	local($lmsg) = @_;
	
	print "
	<HTML><HEAD><TITLE>T-FormMail - Error!!</TITLE></HEAD>
	<BODY BGCOLOR=\"#CCFFCC\">
	<FONT SIZE=\"4\"><B>エラー</B></FONT><P>
	<FONT SIZE=\"3\"><B>$lmsg</B></FONT><P>
	<FONT SIZE=\"3\">ブラウザのBackで戻ってください。</FONT><HR>
	</BODY></HTML>
	";
	
	&tfmlib'lock_check(1);
	
	exit;
}
